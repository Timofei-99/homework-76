const express = require('express');
const router = express.Router();
const fileDb = require('../fileDb');

router.get('/', (req, res) => {
    if (req.query.datetime) {
        const date = new Date(req.query.datetime);
        if (isNaN(date.getDate())) {
            return res.status(400).send('Wrong date!');
        }
        const newMessages = fileDb.getNewMessage(req.query.datetime);
        res.send(newMessages);
    } else {
        const messages = fileDb.getItems();
        res.send(messages);
    }

});

router.post('/', (req, res) => {
    if (req.body.message === '' || req.body.author === '') {
        res.status(400).send({error: 'Not valid data'});
    } else {
        fileDb.addItem(req.body);
        return res.send(req.body);
    }
})


module.exports = router;