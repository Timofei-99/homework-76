const fs = require('fs');
const {nanoid} = require('nanoid');

const path = './messages.json';
let data = [];

module.exports = {
    init() {
        try {
            const files = fs.readFileSync(path);
            data = JSON.parse(files);
        } catch (error) {
            data = [];
        }
    },
    getItems() {
        if (data.length < 30) {
            return data
        } else {
            return data.slice(data.length - 30);
        }
    },

    addItem(item) {
        item.id = nanoid();
        item.datetime = new Date().toISOString();
        data.push(item)
        this.save();
    },

    getNewMessage(datetime) {
        return data.filter((message) => {
            return message.datetime > datetime;
        });
    },

    save() {
        fs.writeFileSync(path, JSON.stringify(data, null, 2));
    }
}