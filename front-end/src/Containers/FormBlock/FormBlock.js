import React, {useState} from "react";
import {Button, Grid, makeStyles, TextareaAutosize, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {postMessage} from "../../store/action/actios";

const useStyles = makeStyles(theme => ({
    inputs: {
        width: '400px',
        marginBottom: theme.spacing(2)
    },
    area: {
        width: '400px'
    }
}));


const FormBlock = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.error)

    const [inputs, setInputs] = useState({
        author: '',
        message: '',
    });

    const changeHandler = (e) => {
        const {name, value} = e.target;
        setInputs(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const message = (e) => {
        e.preventDefault();
        dispatch(postMessage(inputs));
    };


    return (
        <Grid item container direction="column" spacing={2}>
            <form autoComplete="off" onSubmit={message}>
                <Grid item>
                    <TextField
                        name="author" className={classes.inputs}
                        label="Author"
                        onChange={changeHandler}
                    />
                </Grid>
                <Grid item>
                    <TextareaAutosize
                        name="message"
                        onChange={changeHandler}
                        className={classes.area}
                        minRows={5}
                        placeholder="Write message"
                    />
                </Grid>
                <Grid item>
                    <Button onClick={message}>Send message</Button>
                </Grid>
            </form>
        </Grid>
    );
};

export default FormBlock;
