import React, {useEffect} from 'react';
import {Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getMessages, getNewMessages} from "../../store/action/actios";
import Message from "../../Components/Message/Message";
import FormBlock from "../FormBlock/FormBlock";


const Chat = () => {
    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages);
    const lastDate = useSelector(state => state.date);


    useEffect(() => {
        dispatch(getMessages());
        setInterval(() => {
            dispatch(getNewMessages(lastDate))
        }, 2000);
    }, [dispatch]);

    return (
        <Grid>
            <FormBlock/>
            <Typography style={{marginTop: '20px'}} variant="h4">Messages</Typography>
            {messages.map(message => (
                <Message
                    key={message.id}
                    author={message.author}
                    message={message.message}
                    date={message.datetime}
                />
            )).reverse()}
        </Grid>
    );
};

export default Chat;