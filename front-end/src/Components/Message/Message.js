import React from "react";
import {Box, Grid, makeStyles, Typography} from "@material-ui/core";

const styles = makeStyles(theme => ({
    text: {
        margin: '20px',
        padding: '20px',
    },
}));


const Message = ({date, author, message}) => {
    const classes = styles();
    return (
        <Grid item className={classes.block}>
            <Box border={1} className={classes.text}>
                <Typography variant="h6">
                    Author: {author}
                </Typography>
                <p>Message: {message}</p>
                <p>Date: {date}</p>
            </Box>
        </Grid>
    );
};

export default Message;
