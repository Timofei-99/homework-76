import {
    GET_DATE_REQUEST, GET_DATE_SUCCESS,
    GET_MESSAGE_FAILURE,
    GET_MESSAGE_REQUEST,
    GET_MESSAGE_SUCCESS,
    POST_MESSAGE_FAILURE,
    POST_MESSAGE_REQUEST,
    POST_MESSAGE_SUCCESS
} from "../action/actios";

const initialState = {
    messages: [],
    error: null,
    date: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_MESSAGE_REQUEST:
            return {...state}
        case POST_MESSAGE_SUCCESS:
            return {...state}
        case POST_MESSAGE_FAILURE:
            return {...state, error: action.payload}
        case GET_MESSAGE_REQUEST:
            return {...state}
        case GET_MESSAGE_SUCCESS:
            return {...state, messages: action.payload, date: action.payload[action.payload.length - 1].datetime}
        case GET_MESSAGE_FAILURE:
            return {...state}
        case GET_DATE_REQUEST:
            return {...state}
        case GET_DATE_SUCCESS:
            return {...state, messages: action.payload,}
        default:
            return state;
    }
}

export default reducer;