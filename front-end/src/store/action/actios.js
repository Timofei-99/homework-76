import axios from "axios";

export const POST_MESSAGE_REQUEST = 'POST_MESSAGE_REQUEST';
export const POST_MESSAGE_SUCCESS = 'POST_MESSAGE_SUCCESS';
export const POST_MESSAGE_FAILURE = 'POST_MESSAGE_FAILURE';

export const GET_MESSAGE_REQUEST = 'GET_MESSAGE_REQUEST';
export const GET_MESSAGE_SUCCESS = 'GET_MESSAGE_SUCCESS';
export const GET_MESSAGE_FAILURE = 'GET_MESSAGE_FAILURE';

export const GET_DATE_REQUEST = 'GET_DATE_REQUEST';
export const GET_DATE_SUCCESS = 'GET_DATE_SUCCESS';
export const GET_DATE_FAILURE = 'GET_DATE_FAILURE';

export const postMessageRequest = () => ({type: POST_MESSAGE_REQUEST});
export const postMessageSuccess = () => ({type: POST_MESSAGE_SUCCESS});
export const postMessageFailure = (error) => ({type: POST_MESSAGE_FAILURE, payload: error});

export const getMessageRequest = () => ({type: GET_MESSAGE_REQUEST});
export const getMessageSuccess = (messages) => ({type: GET_MESSAGE_SUCCESS, payload: messages});
export const getMessageFailure = () => ({type: GET_MESSAGE_FAILURE});

export const getDateRequest = () => ({type: GET_DATE_REQUEST});
export const getDateSuccess = (newMessage) => ({type: GET_DATE_SUCCESS, payload: newMessage});
export const getDateFailure = () => ({type: GET_DATE_FAILURE});


export const postMessage = (value) => {
    return async (dispatch) => {
        try {
            dispatch(postMessageRequest());
            await axios.post('http://localhost:8000/messages', value);
        } catch (e) {
            dispatch(postMessageFailure());
        }
    }
};

export const getMessages = () => {
    return async (dispatch) => {
        try {
            dispatch(getMessageRequest());
            const response = await axios.get('http://localhost:8000/messages');
            dispatch(getMessageSuccess(response.data));
        } catch (e) {
            dispatch(getMessageFailure(e));
        }
    }
};

export const getNewMessages = (datetime) => {
    return async (dispatch) => {
        try {
            dispatch(getDateRequest());
            const response = await axios.get(`http://localhost:8000/messages?datetime=${datetime}`);
            dispatch(getDateSuccess(response.data));
        } catch (e) {
            dispatch(getDateFailure(e));
        }
    }
}
