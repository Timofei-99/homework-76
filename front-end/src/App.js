import './App.css';
import FormBlock from "./Containers/FormBlock/FormBlock";
import {Container} from "@material-ui/core";
import Chat from "./Containers/Chat/Chat";

function App() {
    return (
        <div className="App">
            <Container>
                <Chat/>
            </Container>
        </div>
    );
}

export default App;
