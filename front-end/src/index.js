import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {applyMiddleware, createStore} from "redux";
import reducer from "./store/reducer/reducer";
import thunk from "redux-thunk";
import {Provider} from "react-redux";

const store = createStore(reducer, applyMiddleware(thunk));

const app = (
    <Provider store={store}>
        <App/>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
